% Created 2018-05-17 Thu 21:10
% Intended LaTeX compiler: pdflatex
\documentclass[11pt]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{grffile}
\usepackage{longtable}
\usepackage{wrapfig}
\usepackage{rotating}
\usepackage[normalem]{ulem}
\usepackage{amsmath}
\usepackage{textcomp}
\usepackage{amssymb}
\usepackage{capt-of}
\usepackage{hyperref}
\usepackage[d]{esvect}
\usepackage{fullpage}
\usepackage{mathtools}
\newcommand*{\RR}{\mathbb{R}}
\newcommand*{\RRn}{\mathbb{R}^n}
\DeclareMathOperator{\Span}{Span}
\DeclareMathOperator{\Null}{Null}
\DeclareMathOperator{\Col}{Col}
\DeclareMathOperator{\LNull}{LNull}
\DeclareMathOperator{\Row}{Row}
\DeclareMathOperator{\rref}{rref}
\DeclareMathOperator{\rank}{rank}
\DeclareMathOperator{\proj}{proj}
\DeclarePairedDelimiter\abs{\lvert}{\rvert}%
\DeclarePairedDelimiter\norm{\lVert}{\rVert}%
\providecommand\given{} % so it exists
\newcommand\SetSymbol[1][]{
\nonscript\,#1\vert \allowbreak \nonscript\,\mathopen{}}
\DeclarePairedDelimiterX\Set[1]{\lbrace}{\rbrace}%
{ \renewcommand\given{\SetSymbol[\delimsize]} #1 }
\author{Brian David Fitzpatrick}
\date{\today}
\title{MIDS Linear Algebra Learning Objectives}
\hypersetup{
 pdfauthor={Brian David Fitzpatrick},
 pdftitle={MIDS Linear Algebra Learning Objectives},
 pdfkeywords={},
 pdfsubject={},
 pdfcreator={Emacs 25.3.1 (Org mode 9.1.13)}, 
 pdflang={English}}
\begin{document}

\maketitle

\section*{Unit I: Vector and Matrix Algebra}
\label{sec:org9ae1b21}

\subsection*{Scalars and Vectors}
\label{sec:org4e7936a}

\begin{itemize}
\item Understand the set notations \(c\in\RR\) and \(\vv{v}\in\RRn\).
\begin{itemize}
\item MC questions on which symbols \(x\) satisfy \(x\in\RR\) and \(x\in\RRn\).
\end{itemize}
\item Understand the geometric interpretation of the coordinate definition of a
vector in \(\RRn\).
\begin{itemize}
\item MC question matching plots of vectors in \(\RR^2\) to their coordinates.
\end{itemize}
\item Understand which physical quantities are measured by scalars and which are
measured by vectors.
\begin{itemize}
\item MC question matching physical quantities to their appropriate tool of
measurement.
\end{itemize}
\end{itemize}

\subsection*{Vector Arithmetic and Terminology}
\label{sec:org2c72025}

\begin{itemize}
\item Understand the algebraic and geometric interpretations of vector length,
normalization, addition, and scalar multiplication.
\begin{itemize}
\item MC questions on computing these operations algebraically.
\item MC questions on identifying these operations geometrically.
\end{itemize}
\item Understand the algebraic and geometric interpretation of a linear combination
of vectors.
\begin{itemize}
\item MC question on setting up the correct algebraic equation to test if
\(\vv{v}\in\Span\Set*{\vv*{v}{1},\dotsc, \vv*{v}{n}}\).
\item MC question on geometrically identifying if
\(\vv{v}\in\Span\Set*{\vv*{v}{1},\dotsc, \vv*{v}{n}}\).
\end{itemize}
\end{itemize}

\subsection*{Vector Inner Products (the Dot Product)}
\label{sec:orge169a3c}

\begin{itemize}
\item Understand the algebraic definition and basic properties of inner products.
\begin{itemize}
\item MC questions on computing expressions involving inner products.
\end{itemize}
\item Understand the \(\vv{v}\cdot\vv{w}\) and \(\vv{v}^\top\vv{w}\) notations.
\begin{itemize}
\item MC question identifying which expressions compute the inner product of two
vectors.
\end{itemize}
\item Understand the inner product interpretation of the length formula for vectors.
\begin{itemize}
\item MC question converting length expressions to inner product expressions.
\end{itemize}
\item Understand the angle formula for inner products.
\begin{itemize}
\item MC question on computing angles between vectors.
\item MC question on determining if a pair of vectors are acute, obtuse, or
orthogonal.
\end{itemize}
\end{itemize}

\subsection*{Matrices and Basic Matrix Operations}
\label{sec:org78e9b25}

\begin{itemize}
\item Understand the basic terminolgy involved in defining a \(m\times n\) matrix.
\begin{itemize}
\item MC question on the size of a matrix.
\item MC question on identifying the \((i, j)\) element of a matrix.
\end{itemize}
\item Understand how to compute matrix sums and scalar-matrix products.
\begin{itemize}
\item MC question on computing these operations.
\item MC question on understanding when these operations are defined.
\end{itemize}
\item Understand the properties of taking linear combinations of matrices.
\begin{itemize}
\item MC question on when a linear combination of matrices is defined.
\end{itemize}
\item Understand how to compute and identify matrix transposes.
\begin{itemize}
\item MC question on computing and idenfitying matrix transposes in a matrix
expression.
\end{itemize}
\end{itemize}

\subsection*{Matrix-Vector Multiplication}
\label{sec:org1d2a210}

\begin{itemize}
\item Understand the "row picture" of matrix-vector multiplication.
\begin{itemize}
\item MC question on computing a matrix-vector product.
\item MC question on which rows of \(A\) are orthogonal to \(\vv{v}\).
\end{itemize}
\item Understand the "column picture" of matrix-vector multiplication.
\begin{itemize}
\item MC question on which linear combination corresponds to a given matrix-vector
product.
\end{itemize}
\item Understand how to convert a system of linear equations into a matrix-vector
product.
\begin{itemize}
\item MC question on which matrix-vector product corresponds to
\(\vv{b}\in\Span\Set*{\vv*{v}{1},\dotsc, \vv*{v}{n}}\).
\end{itemize}
\item Understand the geometric interpretation of matrix-vector multiplication.
\begin{itemize}
\item MC question identifying which matrix converts a given vector into another
given vector.
\end{itemize}
\end{itemize}

\subsection*{Matrix-Matrix Multiplication}
\label{sec:org05717a4}

\begin{itemize}
\item Understand the "row picture" of matrix-matrix multiplication.
\begin{itemize}
\item MC question on computing a matrix-matrix product.
\item MC question on which rows of \(A\) are orthogonal to which columns of \(B\),
given the product \(AB\).
\end{itemize}
\item Understand the "column picture" of matrix-matrix multiplication.
\begin{itemize}
\item MC question on which linear combinations of vectors can be inferred from a
given matrix product.
\end{itemize}
\item Understand the geometric interpretation of \(AB\vv{v}\) as \(A(B\vv{v})\).
\begin{itemize}
\item MC question on which matrix product produces which vector transformation.
\end{itemize}
\end{itemize}

\subsection*{Vector and Matrix Vocabularly}
\label{sec:org0fde13a}

\begin{itemize}
\item Understand each of the following definitions.
\begin{itemize}
\item square matrix
\item column and row matrices
\item diagonal matrices
\item upper and lower triangular matrices
\item symmetric matrices
\item zero vectors and zero matrices
\item identity matrices
\item idempotent matrices
\end{itemize}
\end{itemize}

\section*{Unit II: Systems of Linear Equations and Square Matrices}
\label{sec:org8b6e314}

\subsection*{Augmented Matrices and Reduced Row-Echelon Form}
\label{sec:orgfef742d}

\begin{itemize}
\item Understand how to convert a system of linear equations into an augmented
matrix.
\begin{itemize}
\item MC question on converting a system to an augmented matrix.
\end{itemize}
\item Understand how to identify when an augmented system is in reduced row-echelon
form.
\begin{itemize}
\item MC question on whether or not a given augmented system is in rref.
\end{itemize}
\item Understand how to identify a rref system's rank, nullity, and consistency.
\begin{itemize}
\item MC question on a rref system's rank, nullity, and consistency.
\item MC question on the number of solutions to a rref system.
\end{itemize}
\item Understand how to "find all solutions" to a rref system.
\begin{itemize}
\item MC question on which dependent and free variables in an rref system.
\item MC question on which expression describes all solutions to a rref system.
\end{itemize}
\end{itemize}

\subsection*{Elementary Row-Operations and Elementary Matrices}
\label{sec:org3c03ea9}

\begin{itemize}
\item Understand how to notate and perform each of the following elementary
row-operations.
\begin{itemize}
\item row-switching
\item row-addition
\item row-multiplication
\end{itemize}
\item Understand how to convert each elementary row-operation into an elementary
matrix.
\item Understand how to convert elementary row-operations into elementary matrix
multiplication.
\end{itemize}

\subsection*{Gauss-Jordan Elimination}
\label{sec:org08f9a54}

\begin{itemize}
\item Understand the steps of the Gauss-Jordan algorithm.
\begin{itemize}
\item MC question on the "next step" in a row-reduction in the Gauss-Jordan
algorithm.
\end{itemize}
\item Understand how to use the Gauss-Jordan algorithm to solve a system of linear
equations.
\begin{itemize}
\item MC question on finding all solutions to a system of linear equations.
\item MC question on finding which vectors \(\vv{b}\) yield a consistent system
\(A\vv{x}=\vv{b}\).
\end{itemize}
\item Understand the definitions of rank and nullity of a matrix.
\begin{itemize}
\item MC question on finding the rank and the nullity of a matrix.
\end{itemize}
\end{itemize}

\subsection*{Nonsingular Matrices}
\label{sec:org10288e6}

\begin{itemize}
\item Understand the definition and properties of nonsingularity.
\begin{itemize}
\item MC question about which matrices are nonsingular.
\item MC question about which systems have a unique solution.
\end{itemize}
\item Understand the definition and properties of invertibility.
\begin{itemize}
\item MC question about matrix inverses.
\item MC question about matrix inverse properties.
\end{itemize}
\item Understand how to use row-reductions to compute matrix inverses.
\begin{itemize}
\item MC question about finding matrix inverses.
\end{itemize}
\item Understand that nonsingularity and invertibility are the same.
\begin{itemize}
\item MC question about determining which statements about the fundamental theorem
are equivalent.
\end{itemize}
\end{itemize}

\subsection*{Determinants}
\label{sec:orgb571dfd}

\begin{itemize}
\item Understand the recursive definition of determinants and the laplace cofactor
expansion theorem.
\begin{itemize}
\item MC question about which expression correctly computes the determinant of a
given matrix.
\end{itemize}
\item Understand how to quickly compute determinants of matrices with the following
properties.
\begin{itemize}
\item zero rows or columns
\item triangular matrices
\end{itemize}
\item Understand how to use the following properties of determinants.
\begin{itemize}
\item \(\det(A)=\det(A^\top)\)
\item \(\det(AB)=\det(A)\det(B)\)
\end{itemize}
\item Understand how to efficiently use row-reductions to compute determinants.
\begin{itemize}
\item MC question on computing the determinant of a large matrix.
\end{itemize}
\end{itemize}

\section*{Unit III: Subspaces, Dimension, Orthogonality, and Projections}
\label{sec:org84ae867}

\subsection*{Linear Subspaces of Euclidean Space}
\label{sec:org6caa4c5}

\begin{itemize}
\item Understand the definitions of the four fundamental subspaces attached to a
matrix. (MC questions on how to test if a given vector is in each subspace.)
\begin{itemize}
\item Null Space
\item Column Space
\item Row Space
\item Left Null Space
\end{itemize}
\end{itemize}

\subsection*{Bases and Dimension}
\label{sec:org5b2d2fd}

\begin{itemize}
\item Understand the definition and test for linear independence.
\begin{itemize}
\item MC question about whether or not a list of vectors is linearly independent.
\end{itemize}
\item Understand the definition of a basis for a linear subspace.
\begin{itemize}
\item MC question about whether or not a list of vectors is a basis of a given
linear subspace.
\end{itemize}
\item Understand the dimension theorem and demonstrate the ability to compute the
dimension of a linear subspace.
\begin{itemize}
\item MC question about whether or not two given lists can be a basis for a linear
subspace.
\end{itemize}
\end{itemize}

\subsection*{Bases for the Fundamental Subspaces}
\label{sec:org69c0b0c}

\begin{itemize}
\item Understand the basis algorithms for row spaces and column spaces.
\begin{itemize}
\item MC question on finding a basis for \(\Row(A)\) and \(\Col(A)\) given \(\rref(A)\).
\end{itemize}
\item Understand the basis algorithms for null spaces and left-null spaces.
\begin{itemize}
\item MC question on finding a basis for \(\Null(A)\) given \(\rref(A)\).
\item MC question on finding a basis for \(\LNull(A)\) given \(\rref(A)\).
\end{itemize}
\item Understand the rank-nullity theorem.
\begin{itemize}
\item MC question on finding \(\dim\Null(A)\) and \(\dim\LNull(A)\) given \(\rank(A)\).
\end{itemize}
\end{itemize}

\subsection*{Orthogonality}
\label{sec:org2c88d1a}

\begin{itemize}
\item Understand the definition of orthogonal subspaces and orthogonal complements.
\begin{itemize}
\item MC question on whether or not two linear subspaces are orthogonal or
orthogonal complements.
\end{itemize}
\item Use the relations \(\Row(A)=\Null(A)^\bot\) and \(\Col(A)=\LNull(A)^\bot\) to find
bases for orthogonal complements.
\end{itemize}

\subsection*{Projections}
\label{sec:org9fbc349}

\begin{itemize}
\item Understand the formula for projecting a vector onto a line.
\begin{itemize}
\item MC question on computing \(\proj_{\vv{a}}(\vv{v})\) given
\(\vv{a},\vv{v}\in\RRn\).
\item MC question on computing \(\norm{\proj_{\vv{a}}(\vv{v})}\) given
\(\vv{a},\vv{v}\in\RRn\).
\end{itemize}
\item Understand the formula for finding the matrix projecting onto a linear
subspace.
\begin{itemize}
\item MC question on computing the projection matrix \(P\) onto \(\Col(A)\), where \(A\)
has independent columns.
\item MC question on computing the projection of \(\vv{v}\) onto \(\Col(A)\).
\end{itemize}
\item Understand how to use projections to determine if a linear system is
consistent.
\begin{itemize}
\item MC question on determining if \(A\vv{x}=\vv{b}\) is consistent given \(P\vv{b}\)
where \(P\) is the projection onto \(\Col(A)\).
\end{itemize}
\end{itemize}

\subsection*{Least-Squares Problems}
\label{sec:org1b5188d}

\begin{itemize}
\item Understand how to apply the projection formula to solve least-squares
problems.
\begin{itemize}
\item MC question on finding the "least squares" solution \(\hat{x}\) to
\(A\vv{x}=\vv{b}\).
\end{itemize}
\item Understand how to find the line of "best fit" to nonlinear data.
\begin{itemize}
\item MC question on finding the "least squares" line of regression.
\end{itemize}
\end{itemize}

\subsection*{Orthonormal Bases and the Gram-Schmidt Algorithm}
\label{sec:orgb9214b3}

\begin{itemize}
\item Understand the definition of an orthonormal list.
\begin{itemize}
\item MC question on whether or not a list is orthonormal.
\end{itemize}
\item Understand the Gram-Schmidt algorithm for producing an orthonormal basis of a
linear subspace.
\begin{itemize}
\item MC question on using the "next step" in the Gram-Schmidt algorithm.
\end{itemize}
\item Understand the \$QR\$-factorization.
\begin{itemize}
\item MC question on \$QR\$-factorization.
\end{itemize}
\end{itemize}

\section*{Unit IV: Eigenvalues and Eigenvectors}
\label{sec:org4a92d69}

\subsection*{Basic Notions}
\label{sec:orgb4c7d16}

\begin{itemize}
\item Understand how to use the characteristic polynomial to find the eigenvalues of
a matrix.
\begin{itemize}
\item MC question on computing the characteristic polynomial of a matrix.
\item MC question on finding the eigenvalues given the characteristic polynomial
\end{itemize}
\item Understand how to find bases for the eigenspaces of a matrix.
\begin{itemize}
\item MC question on associating an eigenspace with a null space.
\item MC question on whether or not a given vector is an eigenvector.
\end{itemize}
\item Understand algebraic and geometric multiplicity.
\begin{itemize}
\item MC question on possible algebraic and geometric multiplicities of an
eigenvalue.
\end{itemize}
\end{itemize}

\subsection*{Diagonalization}
\label{sec:org85992c0}

\begin{itemize}
\item Understand how to diagonalize a matrix.
\begin{itemize}
\item MC question on how to diagonalize a matrix.
\end{itemize}
\item Understand how to use diagonalization to compute matrix powers.
\begin{itemize}
\item MC question on computing \(A^k\) given a diagonalization \(A=PDP^{-1}\).
\end{itemize}
\end{itemize}

\subsection*{The Spectral Theorem}
\label{sec:org265c36a}

\begin{itemize}
\item Understanding the statement of the spectral theorem.
\begin{itemize}
\item MC question on whether or not a piece of code produces a valid spectral
decomposition.
\end{itemize}
\item Understand how to use the gram-schmidt algorithm to find a spectral
decomposition of a symmetric matrix.
\begin{itemize}
\item MC question on which stage needs gram-schmidt.
\end{itemize}
\end{itemize}

\subsection*{Definiteness of Quadratic Forms}
\label{sec:org97283c0}

\begin{itemize}
\item Understand the correspondence between symmetric matrices and quadratic forms.
\begin{itemize}
\item MC question matching a quadratic form to its symmetric matrix.
\end{itemize}
\item Understanding the definitions of the definiteness of a quadratic form and
their classification by eigenvalues.
\begin{itemize}
\item MC question on identifying the definiteness of a quadratic form given a
factorization.
\item MC question on identifying the definiteness of a quadratic form given the
eigenvalues.
\end{itemize}
\end{itemize}

\subsection*{SVD}
\label{sec:orga47f6ef}

\begin{itemize}
\item Understand the algorithm for finding the left and right singular vectors and
singular values of a matrix.
\begin{itemize}
\item MC question on which low-rank sum produces a given matrix.
\end{itemize}
\end{itemize}
\end{document}